var spawn = require('child_process').spawn,
    str = '',
    logs = [];
 
 
var b = spawn('git', ['branch', '-r']);
b.stdout.on('data', function(data) { str += data; });
b.on('exit', function() {
  var branches = str.split('\n')
                    .map(function(str) { return str.trim(); })
                    .filter(function(v) { return !!v; })
                    .map(function(branch) { return function(done) { getInfo(branch, done); } });
 
  function getInfo(branch, done) {
    var str = '',
        l = spawn('git', ['log', '-1', branch]);
 
    l.stdout.on('data', function(data) { str += data });
    l.on('exit', function() {
      var lines = str.split('\n'),
          parts,
          date;
      var matched = lines.some(function(line) {
        parts = /Date:\s+(.*)/.exec(line);
        return !!(parts && parts[1]);
      });
      if(!matched) return done();
      date = new Date(parts[1]);
      logs.push({ date: date, log: str, branch: branch });
      done();
    });
  }
 
  // when done, show a summary
  branches.push(function(done) {
    var counts = {};
    logs.forEach(function(item) {
      var year = item.date.getFullYear(),
          month = item.date.getMonth() +1;
      counts[year] || (counts[year] = {});
      if(counts[year][month]) {
        counts[year][month]++;
      } else {
        counts[year][month] = 1;
      }
    });
 
    var names =  ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
 
    Object.keys(counts).forEach(function(year) {
      var months = counts[year];
      Object.keys(months).forEach(function(month) {
        var count = months[month];
        console.log(names[parseInt(month, 10)]+' '+year + ': '+count);
      });
    });
    done();
  });
 
  // when done, show a summary
  branches.push(function(done) {
    var users = {};
    logs.forEach(function(item) {
      var parts = item.branch.split('/');
      if(parts.length > 2) {
        if (users[parts[1]]) {
          users[parts[1]]++;
        } else {
          users[parts[1]] = 1;
        }
      }
    });
 
    var leaderboard = []
    Object.keys(users).forEach(function(user) {
      leaderboard.push({ name: user, count: users[user] });
    });
    leaderboard.sort(function(a, b) { return b.count - a.count; })
               .forEach(function(u) {
                  console.log(u.name +': '+u.count);
               });
    done();
 
  });
 
  // when done, sort logs by date
  branches.push(function(done) {
    logs.sort(function(a, b) { return a.date.getTime() - b.date.getTime(); })
        .forEach(function(item) {
          console.log('\n******** '+item.branch+'\n'+item.log);
        });
    done();
  });
 
  function run(callback) {
    if (callback) {
      callback(function() {
        run(branches.shift());
      });
    }
  }
 
  run(branches.shift());
});
