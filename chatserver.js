net = require("net");
//192.168.0.103

people = [];
s= net.createServer(function (socket) {

  people.push(socket);

  socket.on("data", function (data) {
    for(var i = 0; i < people.length; i++) {
      people[i].write(socket.remoteAddress + ">");
      people[i].write(data);
    }
  });

  socket.on("end", function () {
    var i = people.indexOf(socket);
    people.splice(i, 1);
  });
});

s.listen(8000);
console.log("listening on 8000");
