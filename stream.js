var fs = require('fs');
var http = require('http');

http.createServer(function(req, res) {

  var readStream = fs.createReadStream('../../tmp/flower.jpg');
  
  res.writeHead(200, {'Content-Type': 'image/jpeg'});

  readStream.pipe(res);
}).listen(8080);
