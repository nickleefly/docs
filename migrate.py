#encoding=utf-8
import MySQLdb
import hashlib,re,random,time,os


#TODO config
DBarchive = False
isLab = True
#TODO config  ,, how many tables for new messenger - message table
maxMessageTableNum = 4
TempSaveNums = 1000
maxProcessThreads = 4

__debug = 9
__start = 0
__end = 5
processStep = 10
maxProcessNum = 2


if not isLab:
    __debug = 3
    __start = 0
    __end = 1902
    processStep = 10000
    maxProcessNum = 50
    maxMessageTableNum = 48
    maxProcessThreads = 10

#############################################################
# Functions
#############################################################
#TODO config
def getConnectToOldMessage():
    global isLab
    if isLab :
        conn=getConnections(host="moleman-kaufmich",user="kaufmich",passwd="NetNet2",db="kaufmich")
    else:
        conn=getConnections(host="10.21.1.186",user="kaufmich_read",passwd="Mx6DKYtMpy3KKRNF",db="kaufmich")
    cursor = conn.cursor()
    return conn,cursor

def getConnectToNewMessage():
    global connections,isLab
    if not connections.has_key('threads') :
        if isLab :
            connections['threads'] = getConnections(host="moleman-kaufmich",user="kaufmich",passwd="NetNet2",db="kaufmich_message",ty='_t')
        else:
            connections['threads'] = getConnections(host="10.21.1.150",user="kaufmich_w",passwd="gatEKt9Fh87k",db="kaufmich_msg",ty='_t')
    return connections['threads'],connections['threads'].cursor()

def getConnectToNewMessageTable(tableId):
    global maxMessageTableNum,connections,isLab
    tableId = int(tableId)
    key = 'table'+str(tableId)
    if not connections.has_key(key) :
        if tableId in range(1,maxMessageTableNum+1):  #[1,2,3,4]
            if isLab :
                connections[key] = getConnections(host="moleman-kaufmich",user="kaufmich",passwd="NetNet2",db="kaufmich_message")
            else:
                connections[key] = getConnections(host="10.21.1.150",user="kaufmich_w",passwd="gatEKt9Fh87k",db="kaufmich_msg")
    return connections[key],connections[key].cursor()

connections = {}
def getConnections(host,user,passwd,db,ty='_m'):
    global connections
    key = host+user+passwd+db+ty
    if not connections.has_key(key) :
        connections[key] = MySQLdb.connect(host=host,user=user,passwd=passwd,db=db,charset='utf8')
    return connections[key]


def getHash(id1,id2):
    if(id1>id2):
        return hashlib.md5(str(id2)+'_'+str(id1)).hexdigest()
    else:
        return hashlib.md5(str(id1)+'_'+str(id2)).hexdigest()

#get `threads` by thread_hash  or  create new record
def getTableThreads(uid1,uid2,topic='',attachscount=0):
    global maxMessageTableNum
    uhash = getHash(uid1,uid2)
    connM,cursorM = getConnectToNewMessage()
    cursorM.execute("SELECT * FROM  `threads` where thread_hash=%s",[uhash])
    threadsret = cursorM.fetchone()
    if threadsret is None:
        cursorM.execute("INSERT ignore into `threads` (thread_hash,preview,last_user,users_list,shard_id,has_attachment) values (%s,%s,%s,%s,%s,%s)",[uhash,topic,uid1,str(uid1)+','+str(uid2),random.randint(1,maxMessageTableNum),attachscount])
        cursorM.execute("SELECT * FROM  `threads` where thread_hash=%s",[uhash])
        threadsret = cursorM.fetchone()
        connM.commit()
    #connM.close()
    return threadsret


def getTableThreadsIndex(tid,uid,updated_at='0000-00-00 00:00:00',del_date='0000-00-00 00:00:00'):
    if uid==1 :
        return None
    connM,cursorM = getConnectToNewMessage()
    cursorM.execute("SELECT * FROM  `threads_index` where users_id=%s and threads_id=%s",[uid,tid])
    threadsIndexRet = cursorM.fetchone()
    if threadsIndexRet is None:
        #print '===============NNNN'
        cursorM.execute("INSERT ignore into `threads_index` (users_id,threads_id,unread_count,updated_at,del_date) values (%s,%s,%s,%s,%s)",[uid,tid,0,updated_at,del_date])
        connM.commit()
        cursorM.execute("SELECT * FROM  `threads_index` where users_id=%s and threads_id=%s",[uid,tid])
        threadsIndexRet = cursorM.fetchone()
    #connM.close()
    return threadsIndexRet


def updateTableThreadsIndex(tid,uid,unread_count,del_date,receiverOldUnreadCount):
    if uid == 1:
        return
    if del_date is None:
        del_date = '0000-00-00 00:00:00'
    connM,cursorM = getConnectToNewMessage()
    cursorM.execute("UPDATE `threads_index` SET  `unread_count` =  %s , `del_date`=%s WHERE  `users_id` =%s  AND  `threads_id` =%s ;",[unread_count+receiverOldUnreadCount,del_date,uid,tid])
    connM.commit()
    #connM.close()



#############################################################
# multi-threading
# do insert messages
#############################################################
#import functools
#import threading
#def async(func):
#    @functools.wraps(func)
#    def wrapper(*args, **kwargs):
#        # if args[0]!=[] :
#        #     print 'thread start : uids: ',args[0][0][0],args[0][0][1]
#        my_thread = threading.Thread(target=func, args=args, kwargs=kwargs)
#        #print my_thread
#        my_thread.start()
#    return wrapper
###  #@async

def processMessage(messages):
    global __debug,connections
    if len(messages)>0 :
        #try:
        sender_id = messages[0][0]
        receiver_id = messages[0][1]
        attcount = messages[0][7]
        topic = messages[0][2]
        thread = getTableThreads(sender_id,receiver_id,topic,attcount)
        threadid = thread[0]

        updated_at = messages[0][3] #str(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())))
        # check update_at & del_date & unreadcount
        senderThreadIndex = getTableThreadsIndex(threadid,sender_id,updated_at)
        receiverThreadIndex = getTableThreadsIndex(threadid,receiver_id,updated_at)

        senderThreadIndexDelDate = senderThreadIndex[7] if senderThreadIndex!=None else '_'
        senderOldUnreadCount = senderThreadIndex[2] if senderThreadIndex!=None else 0
        receiverThreadIndexDelDate = receiverThreadIndex[7] if receiverThreadIndex!=None else '_'
        receiverOldUnreadCount = receiverThreadIndex[2] if receiverThreadIndex!=None else 0

        insertData = []
        senderDelDate = None
        senderUnread = 0
        receiverDelDate = None
        receiverUnread = 0
        for message in messages :
            body = message[2]+'<:/subject/:>'+message[5]
            insertData.append( (message[8],threadid,sender_id,message[3],body,message[6]) )
            if not DBarchive :
                if message[4]==8 : #sender deleted
                    senderDelDate = message[3]
                    senderUnread = - senderOldUnreadCount
                elif message[4]==4: #receiver deleted
                    receiverDelDate = message[3]
                    receiverUnread = - receiverOldUnreadCount
                elif message[4] == 0 :
                    receiverUnread+=1

        if not DBarchive :
            # print senderDelDate,receiverDelDate,receiverUnread,'---',threadid,receiver_id
            if senderDelDate is not None :
                updateTableThreadsIndex(threadid,sender_id,senderUnread,senderDelDate,senderOldUnreadCount)
            if receiverDelDate is not None or receiverUnread!=0:
                updateTableThreadsIndex(threadid,receiver_id,receiverUnread,receiverDelDate,receiverOldUnreadCount)


        connM,cursorM = getConnectToNewMessageTable(thread[5])
        if len(insertData)>0 :
            #INSERT ignore into #replace into
            cursorM.executemany("INSERT ignore into messages_"+str(thread[5])+" (id,threads_id,author_id,created_at,message,attachments) values (%s, %s, %s, %s, %s, %s)" ,insertData)
            connM.commit()
            #connM.close()
        if __debug>=4:
            print '-----end','threadid:',threadid,'sender_id:',sender_id,'receiver_id',receiver_id,'count:',len(insertData)
        return True
        #except Exception as inst:
        #    print '--------exception-pid:',os.getpid(),len(messages)
        #    print(type(inst))
        #    return False
        #else:
        #    return True


############################# save many records then execute once
split_ = None
messages = []

def messagesTemp(do,message_id,sender_id,receiver_id,topic,created_at,message_status,body,attachs,attcount):
    global split_,messages,TempSaveNums,maxProcessThreads
    newsplit = str(sender_id)+'-'+str(receiver_id)
    if __debug>=9:
        print '--------messagesTemp',do,sender_id,receiver_id,newsplit,split_
    if len(messages)>=TempSaveNums or newsplit!=split_  or do=='flush':
        param = messages
        messages = []
        #while threading.activeCount()>=maxProcessThreads:
        #    #print 'threading.activeCount()',threading.activeCount()
        #    time.sleep(0.001)
        ret = processMessage(param)
        if not ret :
            messages = messages+param
    if do=='flush':
        if len(messages)>0:
            print '----------------------------re flush----',len(messages)
    else:
        split_ = newsplit
        messages.append([sender_id,receiver_id,topic,created_at,message_status,body,attachs,attcount,message_id])



#############################################################
# main loop
#############################################################
pattern = re.compile('i:(\d{2,});')

def mainLoop(offsetindex=0,processStep=1000):
    offset = offsetindex*processStep
    global pattern,DBarchive
    pmdatabase = '`private_messages`' if not DBarchive else '`cc_private_messages_archive`'
    pmdatabasecontent = '`private_message_contents`' if not DBarchive else '`private_message_contents_archive`'
    print 'pid:',os.getpid(),"SELECT * FROM  "+pmdatabase+" ORDER BY  id asc limit "+str(offset)+","+str(processStep)
    conn,cursor = getConnectToOldMessage()
    #cursor.execute("SELECT * FROM  "+pmdatabase+" where id>??? ORDER BY  id asc limit "+str(offset)+","+str(processStep))
    cursor.execute("SELECT * FROM  "+pmdatabase+" ORDER BY  id asc limit "+str(offset)+","+str(processStep))
    for row in cursor.fetchall():
        if (row[2] is not None) and (row[3] is not None):
            message_id = row[0]
            sender_id = row[2]
            receiver_id = row[3]
            topic = row[8]
            created_at = row[9]
            message_status = row[10]
            cursor.execute("select * from "+pmdatabasecontent+" where id="+str(row[0]))
            content = cursor.fetchone()
            if content:
                body = content[1]
                attachs = ''
                attcount = 0
                if content[2] is not None:
                    #print content[2]
                    match = pattern.findall(content[2])
                    if match:
                        attachs = ','.join(match)
                        attcount = len(match)
                messagesTemp('add',message_id,sender_id,receiver_id,topic,created_at,message_status,body,attachs,attcount)
    if __debug>=8:
        print '===================x==>'
    messagesTemp('flush',0,0,0,0,0,0,0,0,0)
    conn.close()


#############################################################
# multiprocessing  start....
#############################################################
#TODO config
def makeProcess():
    global __start,__end,processStep
    process = Process(target=mainLoop, args=(__start,processStep))
    process.start()
    __start += 1
    return process

from multiprocessing import Process

if __name__ == "__main__":
    tprocess = []
    if maxProcessNum>1:
        while __start<=__end:
            if len(tprocess)<maxProcessNum:
                tprocess.append(makeProcess())
                time.sleep(0.001)
            for k in range(len(tprocess)-1):
                if not tprocess[k].is_alive() :
                    tprocess[k] = makeProcess()
    else:
        makeProcess()
