改IP，可以用 netconfig，可惜每次都得输入完整的IP、掩码、网关和DNS。 不如直接 vi /etc/sysconfig/network-scripts/ifcfg-eth0 再 /etc/init.d/network restart 来得快。

改DNS，最快的就是 vi /etc/resolv.conf，不用重启。

重启httpd，/etc/init.d/httpd restart 要比 service httpd restart快，因为前者可以补齐而后者不能。

修改系统服务，用chkconfig要比setup方便。

看最新的log，用 tail -f /var/log/xxx.log 比 vi /var/log/xxx.log 方便。 看不清的话就先 clear 再 tail -n 0 -f /var/log/xxx.log。

用好bash的历史记录。

压缩解压缩别动不动就tar czvf/tar xzvf，要养成不用v的好习惯。文件多了要快上好几倍。

两台机器传文件，scp 要比samba方便。windows的话就装个cygwin，或者用PuTTY自带的pscp.exe也行。

测试网络连接，nc 192.168.1.10 80 要比 telnet 192.168.1.10 80 好用。

修改samba配置，直接修改 /etc/samba/smb.conf 要比图形界面方便。
