http = require("http");
net = require("net");

sockets = [];
tcpServer = net.createServer(function (socket) {
  socket.write("Hello World\n");
  sockets.push(socket);

  socket.on("end", function (d) {
    var i = sockets.indexOf(socket);
    sockets.splice(i, 1);
  });
});

tcpServer.listen(8001);

s = http.createServer(function (req, res) {
  res.writeHead(200);
  res.write("hello \n");
  res.write("hello \n");
  
  var socket = sockets.shift();
  
  if(socket) {
    socket.on("data", function (d) {
      res.write(d);
    });

    socket.on("end", function (d) {
      res.end("done\n"); 
    });
  } else {
    socket.end("no sockets\n"); 
  }

 
});

s.listen(8000);
